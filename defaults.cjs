function defaults(obj, defaultProps){
    if (typeof obj!='object'){
        return []
    }
    if (typeof defaultProps!='object'){
        return []
    }
    
  
    for (const prop in defaultProps ){

        if(typeof obj.prop=== 'undefined'){
            obj[prop]=defaultProps[prop]
        }
        
        
        
    }
    return obj
    
}

module.exports=defaults