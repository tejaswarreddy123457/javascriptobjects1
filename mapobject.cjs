

function mapobject(obj){
    if (typeof obj!='object'){
        return []
    }
    function callback(temp){
        return temp+'new'
    }    
    for (const i in obj){
        let x= obj[i]
        let v=callback(x)
        obj[i]=v
    }
    return obj

}

module.exports=mapobject